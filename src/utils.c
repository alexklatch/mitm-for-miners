#include <stdio.h>	//For standard things
#include <stdlib.h>	//malloc
#include <string.h>	//memset
#include <netinet/ip_icmp.h>	//Provides declarations for icmp header
#include <netinet/udp.h>	//Provides declarations for udp header
#include <netinet/tcp.h>	//Provides declarations for tcp header
#include <netinet/ip.h>	//Provides declarations for ip header
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <sys/ioctl.h>
#include <linux/if.h>

#include "../include/mitm/utils.h"

void initArray(pools_dyn_array *a, size_t initialSize) {
  a->array = malloc(initialSize * sizeof(cgminer_pool));
  a->used = 0;
  a->size = initialSize;
}

void insertArray(pools_dyn_array *a, cgminer_pool element) {
  // a->used is the number of used entries, because a->array[a->used++] updates a->used only *after* the array has been accessed.
  // Therefore a->used can go up to a->size 
  if (a->used == a->size) {
    a->size *= 2;
    a->array = realloc(a->array, a->size * sizeof(cgminer_pool));
  }
  a->array[a->used++] = element;
}

void freeArray(pools_dyn_array *a) {
  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}

int compare(unsigned char * a, unsigned char * b) {
    int i;

    for(i = 0; i < 6; i++) {
	    if(a[i] != b[i]) {
		    return 1;
        }
    }
    return 0;
}

void print_json(char *json_txt) {
    char *out = NULL;
    cJSON *json = NULL;

    json = cJSON_Parse(json_txt);
    if (!json)
    {
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());
    }
    else
    {
        out = cJSON_Print(json);
        
        cJSON_Delete(json);
        printf("%s\n", out);
        free(out);
    }
}

cJSON *load_json(char *json_txt) {
    cJSON *json = NULL;

    json = cJSON_Parse(json_txt);
    if (!json)
    {
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }
    return json;
}

char* load_file(char *filename) {
    FILE *f = NULL;
    long len = 0;
    char *data = NULL;

    /* open in read binary mode */
    f = fopen(filename,"rb");
    /* get the length */
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    fseek(f, 0, SEEK_SET);

    data = (char*)malloc(len + 1);
    
    fread(data, 1, len, f);
    data[len] = '\0';
    fclose(f);

    return data;
}

int hostname_to_ip(char * hostname , char* ip)
{
	struct hostent *he;
	struct in_addr **addr_list;
	int i;
		
	if ( (he = gethostbyname( hostname ) ) == NULL) 
	{
		// get the host info
		herror("gethostbyname");
		return 1;
	}

	addr_list = (struct in_addr **) he->h_addr_list;
	
	for(i = 0; addr_list[i] != NULL; i++) 
	{
		//Return the first one;
		strcpy(ip , inet_ntoa(*addr_list[i]) );
		return 0;
	}
	
	return 1;
}
