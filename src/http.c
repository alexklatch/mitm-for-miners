#include "../include/mitm/http.h"

struct Request *parse_request(char *raw) {
    struct Request *req;
    req = (struct Request*)malloc(sizeof(struct Request));
    if (!req) {
        return 0;
    }
    memset(req, 0, sizeof(struct Request));

    // Method
    int meth_len = strcspn(raw, " ");
    if (memcmp(raw, "GET", strlen("GET")) == 0) {
        req->method = GET;
    } else if (memcmp(raw, "HEAD", strlen("HEAD")) == 0) {
        req->method = HEAD;
    } else {
        req->method = UNSUPPORTED;
    }
    raw += meth_len + 1; // move past <SP>

    // Request-URI
    int url_len = strcspn(raw, " ");
    req->url = (char*) malloc(url_len + 1);
    if (!req->url) {
        free_request(req);
        return 0;
    }
    memcpy(req->url, raw, url_len);
    req->url[url_len] = '\0';
    raw += url_len + 1; // move past <SP>

    // HTTP-Version
    int ver_len = strcspn(raw, "\r\n");
    req->version = (char*) malloc(ver_len + 1);
    if (!req->version) {
        free_request(req);
        return 0;
    }
    memcpy(req->version, raw, ver_len);
    req->version[ver_len] = '\0';
    raw += ver_len + 2; // move past <CR><LF>

    struct Header *header = 0, *last = 0;
    while (raw[0]!='\r' || raw[1]!='\n') {
        last = header;
        header = (struct Header*) malloc(sizeof(Header));
        if (!header) {
            free_request(req);
            return 0;
        }

        // name
        int name_len = strcspn(raw, ":");
        header->name = (char*) malloc(name_len + 1);
        if (!header->name) {
            free_request(req);
            return 0;
        }
        memcpy(header->name, raw, name_len);
        header->name[name_len] = '\0';
        raw += name_len + 1; // move past :
        while (*raw == ' ') {
            raw++;
        }

        // value
        int value_len = strcspn(raw, "\r\n");
        header->value = (char*) malloc(value_len + 1);
        if (!header->value) {
            free_request(req);
            return 0;
        }
        memcpy(header->value, raw, value_len);
        header->value[value_len] = '\0';
        raw += value_len + 2; // move past <CR><LF>

        // next
        header->next = last;
    }
    req->headers = header;
    raw += 2; // move past <CR><LF>

    int body_len = strlen(raw);
    req->body = (char*) malloc(body_len + 1);
    if (!req->body) {
        free_request(req);
        return 0;
    }
    memcpy(req->body, raw, body_len);
    req->body[body_len] = '\0';


    return req;
}


void free_header(struct Header *h) {
    if (h) {
        free(h->name);
        free(h->value);
        free_header(h->next);
        free(h);
    }
}


void free_request(struct Request *req) {
    free(req->url);
    free(req->version);
    free_header(req->headers);
    free(req->body);
    free(req);
}