#include <stdio.h>	//For standard things
#include <stdlib.h>	//malloc
#include <string.h>	//memset
#include <netinet/ip_icmp.h>	//Provides declarations for icmp header
#include <netinet/udp.h>	//Provides declarations for udp header
#include <netinet/tcp.h>	//Provides declarations for tcp header
#include <netinet/ip.h>	//Provides declarations for ip header
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include "include/stratum/stratum.h"
#include <jansson.h>

#define JSON_LOADS(str, err_ptr) json_loads((str), 0, (err_ptr))

int auth_stratum(struct pool *pool)
{
	json_t *val = NULL, *res_val, *err_val;
	char s[RBUFSIZE], *sret = NULL;
    
	json_error_t err;
	int ret = 0;

	snprintf(s, RBUFSIZE, "{\"id\": %d, \"method\": \"mining.authorize\", \"params\": [\"%s\", \"%s\"]}", 0, pool->rpc_user, pool->rpc_pass);

	if (!stratum_send(pool, s, strlen(s)))
		return ret;

	/* Parse all data in the queue and anything left should be auth */
	while (42) {
		sret = recv_line(pool);
		if (!sret)
			return ret;
		//if (parse_method(pool, sret))
		//	free(sret);
		else
			break;
	}

	val = JSON_LOADS(sret, &err);
	free(sret);
	res_val = json_object_get(val, "result");
	err_val = json_object_get(val, "error");

	if (!res_val || json_is_false(res_val) || (err_val && !json_is_null(err_val)))  {
		char *ss;

		if (err_val)
			ss = json_dumps(err_val, JSON_INDENT(3));
		else
			ss = strdup("(unknown reason)");
		//applog(LOG_INFO, "pool %d JSON stratum auth failed: %s", pool->pool_no, ss);
        printf("pool %d JSON stratum auth failed: %s", pool->pool_no);
		free(ss);

		//suspend_stratum(pool);

		//goto out;
        json_decref(val);
	    return ret;
	}

	ret = 1;
	//applog(LOG_INFO, "Stratum authorisation success for pool %d", pool->pool_no);
    printf("Stratum authorisation success for pool %d", pool->pool_no);
	pool->probed = 1;
	//successful_connect = 1;

	// if (opt_suggest_diff) {
	// 	sprintf(s, "{\"id\": %d, \"method\": \"mining.suggest_difficulty\", \"params\": [%d]}",
	// 		swork_id++, opt_suggest_diff);
	// 	stratum_send(pool, s, strlen(s));
	// }

	json_decref(val);
	return ret;
}

static enum send_ret __stratum_send(struct pool *pool, char *s, ssize_t len)
{
	SOCKETTYPE sock = pool->sock;
	ssize_t ssent = 0;

	strcat(s, "\n");
	len++;

	while (len > 0 ) {
		//struct timeval timeout = {1, 0};
		ssize_t sent;
// 		fd_set wd;
// retry:
// 		FD_ZERO(&wd);
// 		FD_SET(sock, &wd);
// 		if (select(sock + 1, NULL, &wd, NULL, &timeout) < 1) {
// 			if (interrupted())
// 				goto retry;
// 			return SEND_SELECTFAIL;
// 		}

		sent = send(pool->sock, s + ssent, len, MSG_NOSIGNAL);

		if (sent < 0) {
			//if (!sock_blocks())
				return SEND_SENDFAIL;
			//sent = 0;
		}
		ssent += sent;
		len -= sent;
	}

	// pool->cgminer_pool_stats.times_sent++;
	// pool->cgminer_pool_stats.bytes_sent += ssent;
	// pool->cgminer_pool_stats.net_bytes_sent += ssent;
	return SEND_OK;
}

int stratum_send(struct pool *pool, char *s, ssize_t len)
{
	enum send_ret ret = SEND_INACTIVE;

    printf("SEND: %s", s);

	//mutex_lock(&pool->stratum_lock);
	//if (pool->stratum_active)
    ret = __stratum_send(pool, s, len);
	//mutex_unlock(&pool->stratum_lock);

	/* This is to avoid doing applog under stratum_lock */
	switch (ret) {
		default:
		case SEND_OK:
			break;
		case SEND_SELECTFAIL:
			//applog(LOG_DEBUG, "Write select failed on pool %d sock", pool->pool_no);
            printf("Write select failed on pool %d sock", pool->pool_no);
			//suspend_stratum(pool);
			break;
		case SEND_SENDFAIL:
			//applog(LOG_DEBUG, "Failed to send in stratum_send");
            printf("Failed to send in stratum_send");
			//suspend_stratum(pool);
			break;
		case SEND_INACTIVE:
			//applog(LOG_DEBUG, "Stratum send failed due to no pool stratum_active");
            printf("Stratum send failed due to no pool stratum_active");
			break;
	}
	return (ret == SEND_OK);
}


char *recv_line(struct pool *pool)
{
	char *tok, *sret = NULL;
	ssize_t len, buflen;
	int waited = 0;

	if (!strstr(pool->sockbuf, "\n")) {
		//struct timeval rstart, now;

		//cgtime(&rstart);
		//if (!socket_full(pool, DEFAULT_SOCKWAIT)) {
			//applog(LOG_DEBUG, "Timed out waiting for data on socket_full");
		//	goto out;
		//}

		do {
			char s[RBUFSIZE];
			size_t slen;
			ssize_t n;

			memset(s, 0, RBUFSIZE);
			n = recv(pool->sock, s, RECVSIZE, 0);
			if (!n) {
				//applog(LOG_DEBUG, "Socket closed waiting in recv_line");
				//suspend_stratum(pool);
				break;
			}
			//cgtime(&now);
			//waited = tdiff(&now, &rstart);
			if (n < 0) {
				//if (!sock_blocks() || !socket_full(pool, DEFAULT_SOCKWAIT - waited)) {
					//applog(LOG_DEBUG, "Failed to recv sock in recv_line");
					//suspend_stratum(pool);
				//	break;
				//}
			//} else {
				//slen = strlen(s);
				//recalloc_sock(pool, slen);
				//strcat(pool->sockbuf, s);
                printf("ERROR\n");
			}
		} while (waited < DEFAULT_SOCKWAIT && !strstr(pool->sockbuf, "\n"));
	}

	buflen = strlen(pool->sockbuf);
	tok = strtok(pool->sockbuf, "\n");
	if (!tok) {
		//applog(LOG_DEBUG, "Failed to parse a \\n terminated string in recv_line");
		goto out;
	}
	sret = strdup(tok);
	len = strlen(sret);

	/* Copy what's left in the buffer after the \n, including the
	 * terminating \0 */
	if (buflen > len + 1)
		memmove(pool->sockbuf, pool->sockbuf + len + 1, buflen - len + 1);
	else
		strcpy(pool->sockbuf, "");

	// pool->cgminer_pool_stats.times_received++;
	// pool->cgminer_pool_stats.bytes_received += len;
	// pool->cgminer_pool_stats.net_bytes_received += len;
out:
	//if (!sret)
		//clear_sock(pool);
	//else if (opt_protocol)
	//	applog(LOG_DEBUG, "RECVD: %s", sret);
	return sret;
}