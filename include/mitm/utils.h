#ifndef _UTIL_H
#define _UTIL_H

#include "../cJSON.h"
#include "string.h"

// Dynamic array for cgminer pools

typedef struct {
    char*  url;
    char* user;
    char* pass;
} cgminer_pool;

typedef struct {
  cgminer_pool *array;
  size_t used;
  size_t size;
} pools_dyn_array;

void initArray(pools_dyn_array *a, size_t initialSize);

void insertArray(pools_dyn_array *a, cgminer_pool element);

void freeArray(pools_dyn_array *a);

void print_json(char* json_txt);
char *load_file(char *filename);
cJSON *load_json(char* json_txt);

// Method to get ip from hostname
int hostname_to_ip(char * hostname , char* ip);

//Method to compare two char symbols-by-symbols
int compare(unsigned char *a, unsigned char *b);

#endif