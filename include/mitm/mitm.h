#ifndef _MITM_H
#define _MITM_H
#include <linux/if_ether.h>
#include "utils.h"

// Adress Resolution Protocol Structure

struct macip {
    unsigned char mac[6];
    unsigned char ip[4];
};

unsigned char mitm_IPbuf[0xFF];
unsigned char mitm_MACbuf[0xFF];
unsigned char*mitm_printMAC(unsigned char*);
unsigned char*mitm_printIP(unsigned char*);

void mitm_usage();
void mitm_run();
void mitm_end();
void mitm_thread_end(int s);
void *mitm_stratum_spoofer(void*);
void mitm_ARP_cleanup();
void PrintData (unsigned char* data , int Size);
unsigned char *GetData (unsigned char* data , int Size);
void *mitm_sniffer(void*);
void *mitm_printstats(void*);
int mitm_is_victim(unsigned char*);
int mitm_is_me(unsigned char*);
unsigned char *get_mac(char* iface);
int arp_lookup(int,unsigned char*,unsigned char*,unsigned char*);

#endif /* _MITM_H */
