EXEC_NAME = sniffer
OBJ_FILES = src/main.o src/http.o src/utils.o src/url_parser.o src/stratum.o

INCLUDES = -I. -I./include
LIBS = -lpthread -lm -lrt -lcjson -lm -ljansson
LIBS_DIR = -L. -L./lib 
#FOR ARM
#LIBS_DIR = -L. -L./lib/cjson_arm_xilinx_lib
CFLAGS = -Wall -O2 -D_FORTIFY_SOURCE=2
all : $(EXEC_NAME)

install: $(EXEC_NAME)
	@cp sniffer install/
	@cp man/mitm.1.gz /usr/share/man/man1/
 
clean :
	@rm $(EXEC_NAME) $(OBJ_FILES) *~
	@echo "Cleaning OK"

$(EXEC_NAME) : $(OBJ_FILES)
	@export LD_LIBRARY_PATH=$(pwd)/lib:$LD_LIBRARY_PATH
	@${CC} -o out/$(EXEC_NAME) $(OBJ_FILES) $(LIBS_DIR) $(INCLUDES) $(LIBS) 
	@echo "Compiled"
	@cp out/sniffer install/
	@zip -r install.zip install/

%.o: %.c
	@${CC} $(CFLAGS) $(LIBS_DIR) $(INCLUDES) -o $@ -c $< 
	@echo "Compiling $<..."

# FOR ARM XILINX 
#make CC=/tools/Xilinx/SDx/2018.3/gnu/aarch32/5.2.1/lnx64/bin/arm-linux-gnueabihf-gcc